var size = 128
var borderColor = color.FromArgb(111,144,175);
var borderSize = size / 16;
var cell = (size / 2) - (borderSize * 2);

picture.Width = size;
picture.Height = size;

picture.clear(color.White);

for(var i = 0; i < borderSize; i++)
{
  picture.drawRectangle(i, i, size - ((i * 2) + 1), size - ((i * 2) + 1), borderColor);
}

picture.fillRectangle(size - (cell + (borderSize * 2)) , borderSize * 2, cell - 1, cell - 1, borderColor);
picture.fillRectangle(borderSize * 2, size - (cell + (borderSize * 2)), cell - 1, cell - 1, borderColor);
